// if ('production' == process.env.NODE_ENV)
//   require('newrelic');

var restify = require('restify');
var fs = require('fs');
var passport = require('passport');
var authorization = require('./lib/authorization');
var CookieParser = require('restify-cookies');
var corsMiddleware = require('restify-cors-middleware');
var requestIp = require('request-ip');


var server = restify.createServer({
  name: 'Sotong API v1',
  versions: ['1.0.0']
});

server.use(requestIp.mw());
server.use(CookieParser.parse);
server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.bodyParser({ mapParams: true }));
server.use(restify.gzipResponse());
server.use(restify.throttle({
  burst: 100,
  rate: 50,
  ip: true, // throttle based on source ip address
  overrides: {
    '127.0.0.1': {
      rate: 0, // unlimited
      burst: 0
    }
  }
}));


// CORS
var cors = corsMiddleware({
  origins: [
    'http://dev.sotong.co',
    'https://dev.sotong.co',
    'https://sotong.co',
    'http://localhost:3000',
    'http://localhost:5001',
    'https://83f60605.ngrok.io'
  ],
  allowHeaders: [
    'Authorization',
    'Set-Cookie',
    'X-ACCESS-TOKEN',
    'x-access-token',
    'postman-token',
    'cache-control',
    'accept',
    'accept-version',
    'content-type',
    'request-id',
    'origin',
    'x-api-version',
    'x-request-id',
    'x-requested-with',
  ]
});

server.pre(cors.preflight);
server.use(cors.actual);
server.use(function (req, res, next) {
  res.header('Access-Control-Allow-Credentials', true);
  res.charSet('utf-8');
  res.set({ 'content-type': 'application/json' });
  next();
});

// Ping
server.get('/ping', function (req, res, next) {
  res.send(200);
});

// Access Token 인증 미들웨어
server.use(authorization.authorize);

if ('production' != process.env.NODE_ENV)
  server.get('/debug', authorization.debug);

// load route
require('./route.js')(__dirname + '/controllers', server);

server.listen(process.env.PORT || 3333, function startServer() {
  console.log('%s listening at %s', server.name, server.url);
});
