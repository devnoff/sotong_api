const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var RespondModel = function() {};


/** Responds list
 * @param {number}   params.id
 * @param {number}   params.petition_id
 * @param {number}   params.congressman_id
 * @param {string}   params.date_from
 * @param {string}   params.date_to
 * @param {number}   params.limit
 * @param {string}   params.filter
 * @param {fn}       callback(err, result);
 */
RespondModel.prototype.list = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from('sotong.respond','p');

    if (params.id)
      sql = sql.where('id = ?', params.id);
    
    if (params.limit)
      sql = sql.limit(params.limit);

    if (params.petition_id)
      sql = sql.where('petition_id = ?', params.petition_id);

    if (params.congressman_id)
      sql = sql.where('congressman_id = ?', params.congressman_id);

    if (params.date_from !== undefined) {
      sql = sql.where('date_respond > ?', params.date_from);
    }

    if (params.date_to !== undefined) {
      sql = sql.where('date_respond < ?', params.date_to);
    }

    if (params.filter !== undefined) {
      sql = sql.where(`${params.filter} is not null`);
    }

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}

/** 
 * Create a user petition
 * @param   {Object}  params
 * @param   {string}  params.payload.petition_id
 * @param   {string}  params.payload.congressman_id
 * @param   {string}  params.payload.message
 * @param   {number}  params.payload.petition_answer_id
 * @param   {string}  params.payload.type - BY_EMAIL, MANUAL
 * @param   {string}  params.payload.screenshot_url
 * @param   {fn}      callback(err, result) - result : insertId
 */
RespondModel.prototype.create = function(params, callback) {

  // Validation
  if (params.payload == undefined) 
    return callback({error: 'lack of parameter : petition'}, null);

  console.log(params.payload, 'payload');
  var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`sotong.respond`)
              .setFields(params.payload)
              .set('date_respond','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params.payload);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}



module.exports = RespondModel;