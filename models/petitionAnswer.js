const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');


var PetitionModel = function() {};


/** Petition list
 * @param {number}   params.id
 * @param {number}   params.petition_id
 * @param {fn}       callback(err, result);
 */
PetitionModel.prototype.list = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from('sotong.petition_answer','p');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.petition_id)
      sql = sql.where('petition_id = ?', params.petition_id);

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}

/** Single Petition
 * @param {number}   params.id
 * @param {fn}       callback(err, result);
 */

PetitionModel.prototype.get = function(params, callback) {
  this.list(params, function(error, result) {
    callback(error, result ? result[0] : null);
  });
}



module.exports = PetitionModel;