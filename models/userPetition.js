const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var UserPetitionModel = function() {};


/* Update a user petition
 * @param   {Object}  params
 * @param   {string}  params.payload.current_candidate
 * @param   {string}  params.payload.final_candidate
 * @param   {string}  params.payload.favorite_promise
 * @param   {string}  params.payload.dislike_promise
 * @param   {string}  params.payload.message
 * @param   {string}  params.payload.final_reason
 * @param   {array}   params.null_fields
 * @param   {fn}      callback(err, result) - result : insertId
 */
UserPetitionModel.prototype.update = function(params, callback) {

  // Validation
  if (params.payload == undefined) 
    return callback({error: 'lack of parameter : payload'}, null);

  console.log(params.payload);
  var c = new Client(base.db_config);

  try {
    var sql = squel.update({replaceSingleQuotes:true})
              .table(`sotong.user_petition`)
              .setFields(params.payload)
              .set('modified','NOW()',{dontQuote:true})
              .where('user_id = ?', params.payload.user_id)
              ;

    if (params.null_fields && params.null_fields.length > 0) {
      for (var i in params.null_fields) {
        sql = sql.set(params.null_fields[i], 'NULL', {dontQuote:true});
      }
    }

    if (params.sent_message) {
      sql = sql.set('sent_message','NOW()',{dontQuote:true})
    }

  } catch (e) {
    console.log(params.payload);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}



/** 
 * Create a user petition
 * @param   {Object}  params
 * @param   {string}  params.payload.petition_id
 * @param   {string}  params.payload.congressman_id
 * @param   {string}  params.payload.message
 * @param   {string}  params.payload.petition_answer_id
 * @param   {fn}      callback(err, result) - result : insertId
 */
UserPetitionModel.prototype.create = function(params, callback) {

  // Validation
  if (params.payload == undefined) 
    return callback({error: 'lack of parameter : petition'}, null);

  console.log(params.payload, 'payload');
  var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`sotong.user_petition`)
              .setFields(params.payload)
              .set('created','NOW()',{dontQuote:true})
              ;
  } catch (e) {
    console.log(params.payload);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}


/** Get user petition
 * @param   {number}  params.user_id
 * @param   {number}  params.petition_id
 * @param   {number}  params.offset_id
 * @param   {number}  params.limit
 * @param   {object}  params.order - by, asc
 */
UserPetitionModel.prototype.list = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('*')
            .from(`sotong.user_petition`)
            .order('id', false);

  if (params.id !== undefined) 
  sql.where('id = ?', params.id);
            
  if (params.user_id !== undefined) 
    sql.where('user_id = ?', params.user_id);
  
  if (params.petition_id !== undefined) 
    sql.where('petition_id = ?', params.petition_id);

  if (params.offset_id !== undefined) 
    sql.where('id < ?', params.offset_id);

  if (params.offset !== undefined) 
    sql.offset(params.offset);
  
  if (params.limit !== undefined) 
    sql.limit(params.limit);

  if (params.order !== undefined) {
    if (params.order.by) {
      sql.order(params.order.by, params.order.asc != undefined ? params.order.asc : false)
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result);
  });
}


/** Get total number of user petition
 * @param   {number}  params.petition_id
 */
UserPetitionModel.prototype.total = function(params, callback) {

  var c = new Client(base.read_only);

  var sql = squel.select()
            .field('count(*)', 'total')
            .from(`sotong.user_petition`);

  if (params.petition_id !== undefined) 
    sql.where('petition_id = ?', params.petition_id);

  // Query
  base.get(c, sql.toString(), function(error, result) {
    callback(error, result?result[0].total:null);
  });
}


module.exports = UserPetitionModel;
