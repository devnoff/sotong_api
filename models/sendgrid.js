const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');

var SendgridModel = function() {};


/** Sendgrid Event list
 * @param {number}   params.id
 * @param {number}   params.message_id
 * @param {fn}       callback(err, result);
 */
SendgridModel.prototype.list = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from('sotong.sendgrid_event','p');

    if (params.limit !== undefined)
      sql = sql.limit(params.limit);

    if (params.message_id)
      sql = sql.where(`sg_message_id LIKE '%${ params.message_id }%`);

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}

/** 
 * Create a user petition
 * @param   {Object}  params
 * @param   {string}  params.payload.email
 * @param   {string}  params.payload.timestamp
 * @param   {string}  params.payload.smtpid
 * @param   {string}  params.payload.sg_event_id
 * @param   {string}  params.payload.sg_message_id
 * @param   {string}  params.payload.event
 * @param   {fn}      callback(err, result) - result : insertId
 */
SendgridModel.prototype.create = function(params, callback) {

  // Validation
  if (params.payload == undefined) 
    return callback({error: 'lack of parameter : payload'}, null);

  console.log(params.payload, 'payload');
  var c = new Client(base.db_config);

  try {
    var sql = squel.insert({replaceSingleQuotes:true})
              .into(`sotong.sendgrid_event`)
              .setFields(params.payload)
              ;
  } catch (e) {
    console.log(params.payload);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.create(c, sql.toString(), callback);
}



module.exports = SendgridModel;