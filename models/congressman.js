const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');


var CongressmanModel = function () { };


/** Congressman
 * @param {number}   params.id
 * @param {number}   params.name
 * @param {number}   params.party_id
 * @param {array}    params.excludes - ids array
 * @param {fn}       callback(err, result);
 */
CongressmanModel.prototype.list = function (params, callback) {

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
      .field('*')
      .from('sotong.congressman', 'p');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.part_id)
      sql = sql.where('part_id = ?', params.part_id);

    if (params.name)
      sql = sql.where('name LIKE ?', params.name);

    if (params.excludes && Array.isArray(params.excludes)) {
      sql = sql.where('id NOT IN ?', params.excludes);
    }

  } catch (e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function (error, result) {
    callback(error, result);
  });

}

CongressmanModel.prototype.get = function (params, callback) {
  this.list(params, function (error, result) {
    callback(error, result ? result[0] : null);
  });
}



/** Parties
 * @param {fn}   callback(err, result);
 */
CongressmanModel.prototype.parties = function (params, callback) {

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
      .field('*')
      .from('sotong.party', 'p');

  } catch (e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function (error, result) {
    callback(error, result);
  });
}




/** Status
 * @param {number}  petition_id
 * @param {fn}      callback(err, result);
 */
CongressmanModel.prototype.statuses = function (params, callback) {

  // Validation
  if (params.petition_id == undefined)
    return callback({ error: 'lack of parameter : petition_id' }, null);

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
      .field('c.id', 'id')
      .field('c.party_id', 'party_id')
      .field('pa.id', 'petition_answer_id')
      .field('pa.title', 'answer')
      .field('date_sent', 'date_sent')
      .field('date_verified', 'date_verified')
      .field(squel.select().field('count(*)', 'cnt').from('user_petition', 'up').where('up.congressman_id = c.id'), 'petition_cnt')
      .from('congressman', 'c')
      .left_join('respond', 'r', `r.congressman_id = c.id AND r.petition_id = ${params.petition_id}`)
      .left_join('petition_answer', 'pa', 'pa.id = r.petition_answer_id')
      .left_join(
        squel.select()
          .field('congressman_id')
          .field('date_sent')
          .field('date_verified')
          .from(
            squel.select()
              .field('*')
              .from('send')
              .order('date_verified', false), 'send')
          .group('congressman_id'), 's', 's.congressman_id = c.id')

  } catch (e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function (error, result) {
    callback(error, result);
  });

}



/*

.field('ua.identifier', 'auth_email')
            .left_join(squel.select().field('user_id').field('identifier').field('credential').from('sotong.user_auth').where('type LIKE ?','EMAIL'), 'ua', 'u.id = ua.user_id')
            .where('ua.credential LIKE SHA2(?,256)', params.auth.password)
            .where('ua.identifier LIKE ?', params.auth.email);

select
c.id congressman_id,
pa.id petition_answer_id,
pa.title answer,
date_sent,
date_verified,
(select count(*) cnt from user_petition up where up.congressman_id = c.id) received_cnt
from congressman c
left join respond r on r.congressman_id = c.id and r.petition_id=1
left join petition_answer pa on pa.id = r.petition_answer_id
left join (select congressman_id, date_sent, date_verified from (select * from send order by date_verified) send group by congressman_id) s on s.congressman_id = c.id 
*/

module.exports = CongressmanModel;