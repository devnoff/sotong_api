const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');


var SendModel = function() {};


/** Responds list
 * @param {number}   params.id
 * @param {number}   params.petition_id
 * @param {number}   params.congressman_id
 * @param {string}   params.token_key
 * @param {fn}       callback(err, result);
 */
SendModel.prototype.list = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from('sotong.send','s');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.congressman_id)
      sql = sql.where('congressman_id = ?', params.congressman_id);

      if (params.petition_id)
      sql = sql.where('petition_id = ?', params.petition_id);

    if (params.congressman_id)
      sql = sql.where('token_key like ?', params.token_key);

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}


/* Edit send
 * @param 	{Object}	params
 * @param 	{number*}	params.congressman_id
 * @param   {number*}   params.petition_id
 * @param   {number*}   params.token_key
 * @param 	{fn}	    callback(err, result)
 */
SendModel.prototype.verify = function(params, callback) {
  var c = new Client(base.db_config);

  // Validation
  if (params.congressman_id == undefined)
    return callback({ error: "lack of parameter : congressman_id" }, null);
  if (params.petition_id == undefined)
    return callback({ error: "lack of parameter : petition_id" }, null);
  if (params.token_key == undefined)
    return callback({ error: "lack of parameter : token_key" }, null);

  try {
    var sql = squel
      .update({ replaceSingleQuotes: true })
      .table("sotong.send")
      .set("date_verified", "NOW()", { dontQuote: true })
      .where("congressman_id = ?", params.congressman_id)
      .where("petition_id = ?", params.petition_id)
      .where("token_key = ?", params.token_key);

  } catch (e) {
    if (e) {
      console.log(params.user);
      throw e;
    }
  }
  console.log(sql.toString());

  // Query
  base.update(c, sql.toString(), callback);
};


/* Edit user - modify
 * @param 	{Object}	params
 * @param 	{number*}	params.message_id
 * @param 	{string}	params.date_verified
 * @param 	{fn}	    callback(err, result)
 */
SendModel.prototype.update = function(params, callback) {
  var c = new Client(base.db_config);

  // Validation
  if (params.message_id == undefined)
    return callback({ error: "lack of parameter : message_id" }, null);

  try {
    var sql = squel
      .update({ replaceSingleQuotes: true })
      .table("sotong.send")
      .set("date_verified", params.date_verified)
      .where("message_id = ?", params.message_id);

  } catch (e) {
    if (e) {
      console.log(params.user);
      throw e;
    }
  }
  console.log(sql.toString());

  // Query
  base.update(c, sql.toString(), callback);
};


module.exports = SendModel;