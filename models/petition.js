const squel = require('squel');
const Client = require('mariasql');
var base = require('./base');


var PetitionModel = function() {};


/** Candidates
 * @param {number}   params.id
 * @param {number}   params.title
 * @param {boollean} params.ongoing
 * @param {array}    params.excludes - ids array
 * @param {fn}       callback(err, result);
 */
PetitionModel.prototype.list = function(params, callback) {
  // Validation

  var c = new Client(base.read_only);

  try {
    var sql = squel.select()
              .field('*')
              .from('sotong.petition','p');

    if (params.id)
      sql = sql.where('id = ?', params.id);

    if (params.title)
      sql = sql.where('title LIKE ?', params.title);

    if (params.ongoing)
      sql = sql.where('ongoing = ?', params.ongoing ? 1 : 0);

    if (params.excludes && Array.isArray(params.excludes)) {
      sql = sql.where('id NOT IN ?', params.excludes);
    }

  } catch(e) {
    console.log(params);
    if (e) {
      console.log(e);
      throw e;
    }
  }

  // Query
  base.get(c, sql.toString(), function(error, result){
    callback(error, result);
  });

}



module.exports = PetitionModel;