/*
 * libraries 
 */
const async = require('async');
const SendModel = require('../models/send');
const SendgridModel = require('../models/sendgrid');
const moment = require('moment');

/*
 * class declaration
 */


function Event() {}


/*
 * routing
 */

var event = new Event();

module.exports.route = function(app) {
  app.post('/v1/sendgrid/event', event.receiveEvent);
};


/* controller functions */


/** 
 * GET sendgrid/event
 */
Event.prototype.receiveEvent = function(req, res, next) {
  const events = req.body;
  async.waterfall([
    function insert(callback) {
      async.eachSeries(events, function(item, callback_){
        const email = item.email;
        const timestamp = item.timestamp;
        const sg_event_id = item.sg_event_id;
        const sg_message_id = item.sg_message_id;
        const event = item.event;
        const model = new SendgridModel();
        model.create({payload: {
          email,
          timestamp,
          sg_event_id,
          sg_message_id,
          event
        }}, function(error, insertId) {
          callback_(error);
        });
      }, function(error){
        callback(error, events);
      });
    },

    function checkOpens(events, callback) {
      console.log(checkOpens, 'checkOpens');
      async.eachSeries(events, function(item, callback_){
        if (item.event !== 'open') return callback_();
        console.log(item.timestamp, 'item.timestamp');
        const date_verified = moment(item.timestamp).format('YYYY-MM-DD hh:mm:ss');
        const message_id = item.sg_message_id.split('.')[0];
        const model = new SendModel();
        console.log(date_verified, 'date_verified');
        console.log(message_id, 'message_id');
        model.update({
          message_id,
          date_verified
        }, function(error, insertId) {
          callback_(error);
        });
      }, function(error){
        callback(error, events);
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, {success: true});
    } else {
      res.send(error.code, error);
    }
  });
};

