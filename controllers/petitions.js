/*
 * libraries 
 */
const async = require('async');
const colors = require('colors');
const PetitionModel = require('../models/petition');
const cache = require('memory-cache');
const Required = require('../lib/required_helper');


/*
 * class declaration
 */


function Petition() {}


/*
 * routing
 */

var petition = new Petition();

module.exports.route = function(app) {
  app.get('/v1/petitions/ongoing', petition.getOngoingPetition);
  app.get('/v1/petitions/:id/messages', petition.getMessages);
  app.get('/v1/petitions/:id/responds', petition.getResponds);
};


/* controller functions */


/* 
 * Get ongoing petition
 */
Petition.prototype.getOngoingPetition = function(req, res, next) {
  // Cache
  var data = cache.get(`ongoing_petition`);
  if (data) {
    console.log('cached');
    res.setHeader('cached', 'true');
    res.charSet('utf-8');
    res.send(data);
    return;
  }

  async.waterfall([
    function petition(callback) {
      var petitionModel = new PetitionModel(global.version);
      petitionModel.list({
        ongoing: true, 
      }, function(error, petitions){
        if (error) {
          callback({code: 500, description: 'Databse error'}, null);
        } else {
          delete petitions.info;
          var m = [];
          petitions.forEach(function(item, i){
            item.id = parseInt(item.id);
            item.ongoing = parseInt(item.ongoing);
            m.push(item);
          });
          callback(null, m);
        }
      });
    },
    function petitionAnswers(petitions, callback) {
      const petitionAnswerModel = require('../models/petitionAnswer');
      const model = new petitionAnswerModel();
      async.eachSeries(petitions, function(item, callback_){
        model.list({petition_id: item.id}, function(error, result) {
          if (error)
            callback_({code:500, error: {description: 'Database Error', error: error}}, null);
          else {
            item.petition_answers = result;
            delete result.info;
            callback_();
          }
        });
      }, function(err){
        callback(err, petitions);
      });

    }
  ], function(error, result){
    if (error) {
      res.send(error, null);
      cache.put(`ongoing_petition`, result, 60 * 60 * 24 * 1000); // 하루
    } else {
      res.send(200, result);
    }
  });
};


// ------ COMMON FUNC FOR PETITION -------- //
/**
 * @param  {string*}  params.user_id
 * @param  {string*}  params.petition_id
 * @param  {number}   params.offset
 * @param  {number}   params.offset_id
 * @param  {number}   params.limit
 */
function fetchUserPetitions(params, callback) {
  const UserPetitionModel = require('../models/userPetition');
  var model = new UserPetitionModel();
  model.list(params, function(error, result){
    if (error)
      callback({code:500, error: {description: 'Database Error', error: error}}, null);
    else {
      delete result.info;
      callback(null, result);
    }
  });
}


/** 
 * GET petitions/:id/messages
 * @param {number} id - petition id
 * @param {number} limit
 * @param {number} offset_id - id of user petition
 */
Petition.prototype.getMessages = function(req, res, next) {

  // Cache
  var data = cache.get(`recent_messages`);
  if (data) {
    console.log('cached');
    res.setHeader('cached', 'true');
    res.charSet('utf-8');
    res.send(data);
    return;
  }

  async.waterfall([
    function userPetitions(callback) {
      const petition_id = req.params.id;
      const offset = req.params.offset;
      const offset_id = req.params.offset_id;
      const limit = req.params.limit || 10;
      if (limit  > 30) {
        limit = 30;
      }
      const params = {
        petition_id,
        offset,
        offset_id,
        limit
      };

      fetchUserPetitions(params, callback);
    },

    function user(userPetitions, callback) {
      const UserModel = require('../models/user');
      const model = new UserModel();
      async.eachSeries(userPetitions, function(item, callback_){
        model.user({id: item.user_id}, function(error, user) {
          if (error)
            callback_({code:500, error: {description: 'Database Error', error: error}}, null);
          else {
            delete user.info;
            delete item.user_id;
            item.message = item.message
            item.user = {
              id: user.id,
              age: user.age,
              sex: user.sex,
              job: user.job,
              district: user.district
            };
            callback_();
          }
        });
      }, function(err){
        callback(err, userPetitions);
      });
    },

    function congress(userPetitions, callback) {
      const CongressmanModel = require('../models/congressman');
      const model = new CongressmanModel();

      async.eachSeries(userPetitions, function(item, callback_){
        model.list({id: item.congressman_id,}, function(error, result) {
          if (error)
            callback_({code:500, error: {description: 'Database Error', error: error}}, null);
          else {
            delete result.info;
            delete item.congressman_id;
            let cman = result[0];
            item.congressman = {
              id: cman.id,
              name: cman.name,
              party: cman.party
            };
            callback_();
          }
        });
      }, function(err){
        callback(err, userPetitions);
      });
    },

    function stats(userPetitions, callback) {
      const UserPetitionModel = require('../models/userPetition');
      var model = new UserPetitionModel();
      model.total({petition_id: req.params.id}, function(error, total){
        if (error)
          callback({code:500, error: {description: 'Database Error', error: error}}, null);
        else {
          callback(null, {
            data: userPetitions,
            stats: {
              total
            }
          });
        }
      });
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
      cache.put(`recent_messages`, result, 10000); // 10초
    } else {
      res.send(error.code, error);
    }
  });
};


/** 
 * GET petitions/:id/responds
 * @param {number} id - petition id
 * @param {number} limit
 * @param {string} date_from
 * @param {string} date_to
 * @param {string} filter - message
 */
Petition.prototype.getResponds = function(req, res, next) {
  // Cache
  var data = cache.get(`congressman_respond`);
  if (data) {
    console.log('cached');
    res.setHeader('cached', 'true');
    res.charSet('utf-8');
    res.send(data);
    return;
  }

  async.waterfall([
    function respond(callback) {
      const RespondModel = require('../models/respond');
      const model = new RespondModel();

      const param = {
        petition_id: req.params.id,
        limit: req.params.limit,
        date_from: req.params.date_from,
        date_to: req.params.date_to,
        filter: req.params.filter
      };
      model.list(param, function(error, result){
        callback(error, result);
      });
    },
    function congress(responds, callback) {
      const CongressmanModel = require('../models/congressman');
      const model = new CongressmanModel();
      async.eachSeries(responds, function(item, callback_){
        model.list({id: item.congressman_id}, function(error, result) {
          if (error)
            callback_({code:500, error: {description: 'Database Error', error: error}}, null);
          else {
            delete result.info;
            delete item.congressman_id;
            let cman = result[0];
            item.congressman = {
              id: cman.id,
              name: cman.name,
              party_id: cman.party_id
            };
            callback_();
          }
        });
      }, function(err){
        callback(err, responds);
      });
    },
    function petitionAnswers(responds, callback) {
      const petitionAnswerModel = require('../models/petitionAnswer');
      const model = new petitionAnswerModel();
      async.eachSeries(responds, function(item, callback_){
        model.get({id: item.petition_answer_id}, function(error, result) {
          if (error)
            callback_({code:500, error: {description: 'Database Error', error: error}}, null);
          else {
            let pa = result;
            item.petition_answer = {
              id: item.petition_answer_id,
              title: pa.title,
              short_title: pa.short_title
            };

            delete result.info;
            delete item.petition_answer_id;
            callback_();
          }
        });
      }, function(err){
        callback(err, responds);
      });

    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
      cache.put(`congressman_respond`, result, 10000); // 10초
    } else {
      res.send(error.code, error);
    }
  });
};