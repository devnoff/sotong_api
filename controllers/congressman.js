/*
 * libraries 
 */
const async = require('async');
const colors = require('colors');
const CongressmanModel = require('../models/congressman');
const cache = require('memory-cache');
const _ = require('lodash');
const { normalize, schema } = require('normalizr');
const Required = require('../lib/required_helper');

/*
 * class declaration
 */


function Congressman() {}


/*
 * routing
 */

var congressman = new Congressman();

module.exports.route = function(app) {
  app.get('/v1/congressmans/petitions/:petition_id/status/all', congressman.getRichStatus);
  app.get('/v1/congressmans/petitions/:petition_id/status', congressman.getStatus);
  app.get('/v1/congressmans', congressman.getCongressmans);

  // 메일 응답 처리
  app.get('/v1/congressmans/:id/email-respond', congressman.getEmailRespond);
  app.get('/v1/congressmans/:id/email-open.jpg', congressman.setEmailOpen);
};


/* controller functions */


/** 
 * GET congressmans/petitions/:petition_id/status/all
 * @param {number} petition_id
 */
Congressman.prototype.getRichStatus = function(req, res, next) {

  // Cache
  var data = cache.get(`congress_status_rich`);
  if (data) {
    console.log('cached');
    res.setHeader('cached', 'true');
    res.charSet('utf-8');
    res.send(data);
    return;
  }

  const model = new CongressmanModel();
  async.waterfall([
    function party(callback) {
      model.parties({}, function(error, result) {
        delete result.info;
        callback(error, result);
      });
    },

    function congress(parties, callback) {
      console.info('---------- Fetching congress ---------- ');
      model.list({}, function(error, result) {
        delete result.info;
        callback(error, parties, result);
      });
    },

    function status(parties, congressman, callback) {
      console.info('---------- Fetching status ---------- ');
      const petition_id = req.params.petition_id;
      model.statuses({ petition_id }, function(error, result) {
        delete result.info;
        callback(error, parties, congressman, result);
      });
    },

    function congress(parties, congressmans, statuses, callback) {
      console.info('---------- congress ---------- ');

      parties.forEach(party => {
        party.congressmans = [];
      });

      congressmans.forEach(cman => {
        const p = _.find(parties, {id: cman.party_id});
        const s = _.find(statuses, {id: cman.id});
        console.log(p, 'p');
        console.log(s, 's');
        const id = cman.id;
        const name = cman.name;
        p.congressmans.push(Object.assign({}, {id, name}, {status: s}));
      });

      callback(null, parties);
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
      cache.put(`congress_status_rich`, result, 200000); // 5분
    } else {
      res.send(error.code, error);
    }
  });
};


/** 
 * GET congressmans/petitions/:petition_id/status
 * @param {number} petition_id
 * @param {string} format - {nested(default), normalized}
 * 
 * @todo memory cache
 */
Congressman.prototype.getStatus = function(req, res, next) {
  // Cache
  var data = cache.get(`congress_status`);
  if (data) {
    console.log('cached');
    res.setHeader('cached', 'true');
    res.charSet('utf-8');
    res.send(data);
    return;
  }

  const model = new CongressmanModel();
  async.waterfall([
    function status(callback) {
      console.info('Fetching status');
      const petition_id = req.params.petition_id;
      model.statuses({ petition_id }, function(error, result) {
        delete result.info;
        callback(error, result);
      });
    },
    function option(statuses, callback) {
      if (req.params.format !== undefined && req.params.format === 'normalized') {
        const c = new schema.Entity('congressman');
        const s = new schema.Entity('status', {
          congressmans: [c]
        });

        const normalizedData = normalize({congressmans: statuses}, s);
        callback(null, normalizedData['entities']['congressman']);
      } else {
        callback(null, statuses);
      }
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
      cache.put(`congress_status`, result, 200000); // 5분
    } else {
      res.send(error.code, error);
    }
  });
};


/** 
 * GET congressmans
 * @param {number} petition_id
 * @param {string} format - {nested(default), normalized}
 * 
 * @todo memory cache
 */
Congressman.prototype.getCongressmans = function(req, res, next) {
  const model = new CongressmanModel();
  async.waterfall([
    function party(callback) {
      model.parties({}, function(error, result) {
        delete result.info;
        callback(error, result);
      });
    },
    function congressman(parties, callback) {
      console.info('Fetching congress');
      model.list({} , function(error, result) {
        delete result.info;
        result.forEach(item => {
          let i = _.findIndex(parties, {id: item.party_id});
          item.party = {
            id: parseInt(parties[i].id),
            name: parties[i].name
          };
          delete item.party_id;
        });
        callback(error, result);
      });
    },
    function option(statuses, callback) {
      if (req.params.format !== undefined && req.params.format === 'normalized') {
        const c = new schema.Entity('congressman');
        const s = new schema.Entity('status', {
          congressmans: [c]
        });

        const normalizedData = normalize({congressmans: statuses}, s);
        callback(null, normalizedData['entities']['congressman']);
      } else {
        callback(null, statuses);
      }
    }
  ], function(error, result){
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};



/**
 * 이메일 응답 처리
 * /v1/congressmans/:id/respond/email-respond
 * 
 * @param {string} token
 * @param {string} answer_id
 */

Congressman.prototype.getEmailRespond = function(req, res, next) {
  const RespondModel = require('../models/respond');
  const SendModel = require('../models/send');
  async.waterfall([
    function validation(callback) {
      Required.all(['token', 'answer_id'], req.params, callback);
    },
    function findSend(params, callback) {
      const model = new SendModel();
      const token_key = params.token;
      const congressman_id = req.params.id;
      console.info('Fetching send');
      model.list({congressman_id, token_key} , function(error, result) {
        delete result.info;
        if (error) {
          callback({code:500, message: 'Database Error'}, null);
        } else {
          callback(null, result[0], params)
        }
      });
    },
    function checkExist(send, params, callback) {
      var model = new RespondModel();
      model.list({congressman_id: params.id, petition_id: send.petition_id}, function(error, result){
        delete result.info;
        if (error) {
          callback({code:500, message: 'Database Error'}, null);
        } else {
          if (result[0]) {
            callback({code: 208, message: 'You have already answered'}, null);
          } else {
            callback(null, send, params);
          }
        }
      });
    },
    function process(send, params, callback) {
      if (send) {
        // 응답 생성
        var model = new RespondModel();
        var payload = {
          petition_id: send.petition_id,
          congressman_id: send.congressman_id,
          petition_answer_id: params.answer_id,
          type: 'BY_EMAIL'
        };
        model.create({payload}, function(error, insertId) {
          if (error) {
            callback({code:500, message: 'Database Error'}, null);
          } else {
            callback(null, insertId)
          }
        });
      } else {
        callback({code: 400, message: 'Unauthorized'}, null);
      }
    }
  ], function(error, result){
    if (!error) {
      var body = '<html lang="ko"><head><meta charset="utf-8"></head><body><script>alert("의원님의 소중한 답변 감사합니다!");location.href="http://sotong.co"</script></body></html>';
      res.writeHead(200, {
        'Content-Length': Buffer.byteLength(body),
        'Content-Type': 'text/html'
      });
      res.write(body);
      res.end();
    } else {
      if (error.code == 208) {
        var body = '<html lang="ko"><head><meta charset="utf-8"></head><body><script>alert("이미 답변을 하셨습니다!");location.href="http://sotong.co"</script></body></html>';
        res.writeHead(200, {
          'Content-Length': Buffer.byteLength(body),
          'Content-Type': 'text/html'
        });
        res.write(body);
        res.end();
      }
      else 
        res.send(error.code, error);
    }
  });
};


/**
 * 이메일 확인 처리
 * /v1/congressmans/:id/respond/email-open
 * 
 * @param {string} token
 */

Congressman.prototype.setEmailOpen = function(req, res, next) {
  const RespondModel = require('../models/respond');
  const SendModel = require('../models/send');
  async.waterfall([
    function validation(callback) {
      Required.all(['token'], req.params, callback);
    },
    function findSend(params, callback) {
      const model = new SendModel();
      const token_key = params.token;
      const congressman_id = req.params.id;
      console.info('Fetching send');
      model.list({congressman_id, token_key} , function(error, result) {
        delete result.info;
        if (error) {
          callback({code:500, message: 'Database Error'}, null);
        } else {
          callback(null, result[0], params)
        }
      });
    },
    
    function process(send, params, callback) {
      if (send) {
        // 읽음 확인 업데이트
        var model = new SendModel();
        var payload = {
          petition_id: send.petition_id,
          congressman_id: send.congressman_id,
          token_key: params.token,
        };
        model.verify(payload, function(error, result) {
          if (error) {
            callback({code:500, message: 'Database Error'}, null);
          } else {
            callback(null, result)
          }
        });
      } else {
        callback({code: 400, message: 'Unauthorized'}, null);
      }
    }
  ], function(error, result){
    if (!error) {
      res.setHeader('content-type', 'image/png');
      res.send(200);
    } else {
      res.send(500, error);;
    }
  });
};