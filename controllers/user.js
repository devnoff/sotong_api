/*
 * libraries 
 */
const UserModel = require('../models/user');
const async = require('async');
const colors = require('colors');
const cache = require('memory-cache');
const _ = require('lodash');
const Required = require('../lib/required_helper');
const UserPetitionModel = require('../models/userPetition');
const PetitionModel = require('../models/petition');

/*
 * class declaration
 */


function User() { }


/*
 * routing
 */

var user = new User();

module.exports.route = function (app) {
  app.get('/v1/users', user.getUsers);
  app.get('/v1/users/:id', user.getUser);
  app.put('/v1/users/:id', user.setUser);

  // User Petition
  app.get('/v1/users/:id/petitions', user.getPetitions);
  app.post('/v1/users/:id/petitions/:petition_id', user.createPetition);

};


/*
 * controller functions
 */


/*
 * GET users  Collection of users
 * @param   {nubmer}  offset
 * @param   {nubmer}  limit
 */
User.prototype.getUsers = function (req, res, next) {

  if (['ADMIN'].indexOf(Session.permission(req)) < 0) {
    return res.send(401, { error: "Not permitted" });
  }


  var userModel = new UserModel();

  var params = {
    offset: req.params.offset,
    limit: req.params.limit,
    status: req.params.status
  }

  async.waterfall([
    function (callback) {
      userModel.users(params, function (error, users) {
        if (users) {
          callback(null, users);
        } else {
          callback({ error: error }, null);
        }
      });
    }
  ], function (error, result) {
    if (!error) {
      res.send(200, result);
    } else {
      res.send(500, error);
    }
  });
  return next();
};

/*
 * GET users/:id  User object
 *
 */
User.prototype.getUser = function (req, res, next) {
  var userModel = new UserModel();
  var params = { id: req.params.id };
  async.waterfall([
    function (callback) {
      userModel.user(params, function (error, user) {
        if (user) {
          callback(null, user);
        } else {
          callback({ error: error }, null);
        }
      });
    }
  ], function (error, result) {
    if (!error) {
      res.send(200, result);
    } else {
      res.send(500, error);
    }
  });
  return next();
};


/**
 * POST  users/:id  Modify user
 * @param {string}  name
 * @param {number}  age
 * @param {string}  gender
 * @param {number}  location_id
 * @param {string}  job
 * @param {boolean} married - 0,1 
 * @param {boolean} has_children - 0,1
 * @param {number}  withdraw_request - 0,1
 */
User.prototype.setUser = function (req, res, next) {

  console.info(colors.cyan('Set User'));

  // Check permission
  if ('production' == process.env.NODE_ENV && 'production' == process.env.NODE_ENV && req.user.id != req.params.id)
    return res.send(403, { code: 403, description: "forbidden" });

  var userModel = new UserModel();

  var user = {
    id: req.params.id
  };

  if (req.params.name !== undefined)
    user.name = req.params.name;

  if (req.params.age !== undefined)
    user.age = req.params.age;

  if (req.params.sex !== undefined)
    user.sex = req.params.sex;

  if (req.params.district !== undefined)
    user.district = req.params.district;

  if (req.params.job !== undefined)
    user.job = req.params.job;

  async.waterfall([
    function update(callback) {
      console.info(colors.cyan('Set User : UPDATE'));

      userModel.update({ user: user }, function (error, info) {
        if (error) {
          callback({ code: 500, description: "Update failed", error: error }, null);
        } else {
          callback(null, info);
        }
      });
    },
    function get(info, callback) {
      console.info(colors.cyan('Set User : GET'));

      userModel.get({ id: user.id }, function (error, user) {
        if (error) {
          callback({ code: 500, description: "Database Error", error: error }, null);
        } else {
          if (!user) {
            callback({ code: 404, description: "Not found", error: error }, null);
          } else {
            callback(null, user);
          }
        }
      });
    }
  ], function (error, result) {
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
  return next();
};



// ------ COMMON FUNC FOR PETITION -------- //
/**
 * @param  {string*}  params.id  // user_petition_id
 * @param  {string*}  params.user_id
 * @param  {string*}  params.petition_id
 */
function fetchUserPetitions(params, callback) {
  var model = new UserPetitionModel();
  model.list(params, function (error, result) {
    if (error)
      callback({ code: 500, error: { description: 'Database Error', error: error } }, null);
    else {
      delete result.info;
      callback(null, result);
    }
  });
}



/*
 * GET users/:id/petitions
 */
User.prototype.getPetitions = function (req, res, next) {

  if ('production' == process.env.NODE_ENV && 'production' == process.env.NODE_ENV && req.user.id != req.params.id)
    return res.send(403, { code: 403, description: "forbidden" });

  async.waterfall([
    function userPetitions(callback) {
      const user_id = req.params.id;
      fetchUserPetitions({ user_id }, callback);
    },

    function congressman(userPetitions, callback) {
      const CongressmanModel = require('../models/congressman');
      const model = new CongressmanModel();
      async.eachSeries(userPetitions, function (item, callback_) {
        model.get({ id: item.congressman_id }, function (error, result) {
          if (error)
            callback_({ code: 500, error: { description: 'Database Error', error: error } }, null);
          else {
            item.congressman = {
              id: result.id,
              name: result.name
            };
            delete result.info;
            delete item.congressman_id;
            callback_();
          }
        });
      }, function (err) {
        callback(err, userPetitions);
      });
    },

    function petition(userPetitions, callback) {
      var model = new PetitionModel();
      async.eachSeries(userPetitions, function (item, callback_) {
        model.list({ id: item.petition_id }, function (error, result) {
          if (error)
            callback_({ code: 500, error: { description: 'Database Error', error: error } }, null);
          else {
            delete result.info;
            delete petition_id;
            item.petition = result;
            callback_();
          }
        });
      }, function (err) {
        callback(err, userPetitions);
      });
    }
  ], function (error, result) {
    if (!error) {
      res.send(200, result);
    } else {
      res.send(error.code, error);
    }
  });
};



/**
 * GET users/:id/petitions
 * @param   {string*}  id - user id
 * @param   {string*}  petition_id
 * @param   {string*}  congressman_id
 * @param   {string*}  message
 * @param   {string}   age
 * @param   {string}   sex
 * @param   {string}   job
 * @param   {string}   district
 * @param   {string}   petition_answer_id
 */
User.prototype.createPetition = function (req, res, next) {

  if ('production' == process.env.NODE_ENV && req.user.id != req.params.id)
    return res.send(403, { code: 403, description: "forbidden" });

  const UserPetitionModel = require('../models/userPetition');

  let params = JSON.parse(req.body);
  params = Object.assign({}, params, req.params);
  console.log(params);

  async.waterfall([
    function validate(callback) {
      Required.all(['petition_id', 'congressman_id', 'message'], params, callback);
    },
    function checkExist(validParams, callback) {
      const payload = {
        user_id: params.id,
        petition_id: params.petition_id
      };
      fetchUserPetitions(payload, function (error, result) {
        if (error)
          callback({ code: 500, error: { description: 'Database Error', error: error } }, null);
        else {
          if (result.length > 0) {
            callback({ code: 400, error: { description: 'Exceed MAX petition limit(max: 1)' } }, null);
          } else {
            callback(null, params);
          }
        }
      });
    },
    function create(params, callback) {
      var model = new UserPetitionModel();
      var payload = {
        user_id: parseInt(params.id),
        petition_id: parseInt(params.petition_id),
        congressman_id: parseInt(params.congressman_id),
        message: params.message,
        petition_answer_id: params.petition_answer_id ? parseInt(params.petition_answer_id) : null,
      };
      model.create({ payload }, function (error, insertId) {
        if (error)
          callback({ code: 500, error: { description: 'Database Error', error: error } }, null);
        else {
          callback(null, { id: insertId });
        }
      });
    },

    fetchUserPetitions,

    function updateUserInfo(userPetition, callback) {
      var userModel = new UserModel();
      var user = {
        id: parseInt(params.id),
        age: params.age || null,
        sex: params.sex || null,
        job: params.job || null,
        district: params.district || null
      }
      userModel.update({ user }, function(error, result){
        callback(error, userPetition, user);
      });
    }
  ], function (error, user_petitions, user) {
    if (!error) {
      res.send(200, {user_petitions, user});
    } else {
      res.send(error.code, error);
    }
  });
};
