var jwt = require('jwt-simple');
var moment = require('moment');
var passport = require('passport');
var async = require('async');
const secret = process.env.JWT_SECRET;
const default_age = 60*60*24*7; // 24 hour * 7


function validate(decodedToken, req) {
    if(process.env.NODE_ENV == 'development') {
      return true;
    }

    console.log(decodedToken, 'Decoded Token');
    let valid = true;
    try {
        const now = moment().unix();
        // 유효기간
        if (decodedToken.exp < now) {
        console.log(now, decodedToken.exp, 'Expired Token');
        valid = false;
        }

        if (decodedToken.ip != req.clientIp) {
        console.log("Invalid token: ip doesn't match" );
        console.log(req.clientIp, 'Client IP');
        console.log(decodedToken.ip, 'Token IP');
        valid = false;
        }
    } catch (e) {
        valid = false;
    }
    return valid;
  }

function generateToken(req, user) {
  var now = moment().unix();
  var expire = now + default_age;
  var token = {
    created: now,
    origin: req.headers['origin'],
    ip: req.clientIp,
    iss: 'sotong.co',
    exp: expire,
    user: {
      id: user.id
    }
  };
  console.log(token, 'TOKEN CREATED');
  return {
    access_token: jwt.encode(token, secret),
    expires: expire
  };
}


module.exports = {
  generateToken : function(req, user) {
    return generateToken(req, user);
  },
  // 매 요청시에 토큰이 유효한지 확인하는 미들웨어
  authorize: function(req, res, next) {
    // SKIP : 페이스북 로그인 및 핑 시에
    if (['/v1/auth/facebook','/ping','/'].indexOf(req.url) > -1) {
      return next();
    }

    var BearerStrategy = require('passport-http-bearer').Strategy;
    passport.use(new BearerStrategy(function(token, done) {

      var decodedToken = jwt.decode(token, secret);

      // Validate Token
      if ('production' == process.env.NODE_ENV && !validate(decodedToken, req)) {
        return done({description: "Invalid Token"}, null);
      }

      // Get User
      const UserModel = require('../models/user');
      var userModel = new UserModel();
      var user = decodedToken.user;
      userModel.user({id: user.id}, function(error, user){
        if (user) {
          done(null, user);
        } else {
          done({description:'Database Error', error: error}, null);
        }
      });
      
    }));

    passport.authenticate('bearer', {session: false}, function(err, user, info) {
      if (err) {
        console.log(err, 'Authorization Error');
        res.send(401, 'Unauthorized');
      } else {
        // 사융자 객체를 요청객체에 담음
        req.user = user;
        return next();
      }
    })(req, res);
  },
  validate: function(token, req) {
    var decodedToken = jwt.decode(token, secret);
    return validate(decodedToken, req);
  },
  userFromToken: function(token) {
    var decodedToken = jwt.decode(token, secret);
    return decodedToken.user;
  },
  debug: function(req, res, next) {
    
    var token = req.params.token;
    var decodedToken = jwt.decode(token, secret);

    res.send(200, {
      token: token,
      decodedToken: decodedToken
    });
  }
};